export interface Response<I> {
    status: number;
    message: string;
    data: I;
    meta?: Meta;
}

export interface Meta {
    p: number;
    limit: number;
    total: number;
    numPages: number;
}

export interface Id {
    id: string;
}