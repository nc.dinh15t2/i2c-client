import { Image } from './image.model';

export interface ShortUser {
    _id: string;
    username: string;
    name: {
        first: string;
        last: string;
    };
    email: string;
    avatar: Image;
}

export interface FullUser extends ShortUser {
    created: number;
}