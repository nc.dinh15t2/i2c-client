export enum ImageSize {
    SMALL = 'small',
    MEDIUM = 'medium',
    LARGE = 'large'
};

export interface Size {
    localName: string;
    size: {
        width: number;
        height: number;
    }
}

export interface Image {
    _id?: string;
    domain?: string;
    url: string;
    small: Size;
    medium: Size;
    large: Size;
}