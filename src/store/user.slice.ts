import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { FullUser } from "../models";
import { RootState } from "./store";

export const userSlice = createSlice({
    name: 'user',
    initialState: { value: null as FullUser },
    reducers: {
        setUser: (state, action: PayloadAction<FullUser>) => {
            state.value = action.payload; 
        }
    }
});

export const { setUser } = userSlice.actions;

export const selectUser = (state: RootState) => state.user.value; 

export const userReducer = userSlice.reducer;