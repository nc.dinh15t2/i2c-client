import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { ImageSize, ShortUser } from '../models';
import { getUsers } from '../services';
import { getImageUrl, RouteUrl } from '../utils';
import './css/AdminUser.css';

export function AdminUser() {
    const pageSize = 10;
    const [page, setPage] = useState(0);
    const [users, setUsers] = useState<ShortUser[]>([]);

    const meta = { "p": 0, "limit": 10, "total": 3, "numPages": 1 };
    
    const fetchUsers = () => {
        getUsers(page, pageSize)
            .then(response => {
                console.log(response);
                setUsers(response.data.data);
            })
            .catch(err => {
                console.log(err);
            });
    }

    useEffect(() => {
        fetchUsers();
    }, []);

    const getUserRows = () => {
        let index = 0;
        return users.map((user) => {
            index ^= 1;
            return (
                <div className={"table-row " + (index?"bg-white": "bg-lightgray")} key={user._id}>
                    <div className="p-0">
                        <input type="checkbox" className="form-check-input" />
                    </div>
                    <div className="text-left">
                        <Link to={'/profile?user_id=' + user._id}>{user.username}</Link> 
                    </div>
                    <div>
                        {user.email}
                    </div>
                    <div>
                        {user.name.last}
                    </div>
                    <div>
                        {user.avatar?
                        <img src={getImageUrl(user.avatar, ImageSize.SMALL)}/>
                        :'No image'}
                    </div>
                    <div>
                        <Link className="btn btn-success p-1" to={'/admin_profile?user_id=' + user._id}>
                            Edit
                        </Link>
                    </div>
                </div>
            )
        })
    }
    return (
        <div className="mx-2 my-3 w-100 h-100">
            <div className="d-flex justify-content-start">
                <button className="btn btn-primary mx-2">Add</button>
                <button className="btn btn-danger">Delete</button>
            </div>
            <div className="mt-3 mx-2 table-wrapper">
                <div className="table-row bg-info bg-gradient fw-bold">
                    <div>
                    </div>
                    <div>
                        Username
                    </div>
                    <div>
                        Email
                    </div>
                    <div>
                        Name
                    </div>
                    <div>
                        Avatar
                    </div>
                    <div>
                        Action
                    </div>
                </div>
                { getUserRows() }
            </div>
        </div>
    );
}