import './css/Auth.css';

export function Auth({children}) {
    return (
        <div className="auth-wrapper p-5">
            { children }
        </div>
    );
}