import { useRef, useState } from 'react';
import { MdAddAPhoto } from 'react-icons/md';
import { Image, ImageSize, UploadCertificate } from '../models';
import { getImage, signUploadCertificate, updateAvatar, uploadImage } from '../services';
import { getImageUrl, UploadType } from '../utils';
import './css/AvatarComponent.css';

export function AvatarComponent({ editable, avatar, userId, onChangeAvatar }) {

    const file: any = useRef();
    const onUpload = async () => {
        const uploadCertificateResp = await signUploadCertificate(UploadType.IMAGE);
        const uploadCertificate: UploadCertificate = uploadCertificateResp.data.data;

        const uploadImageResp =
            await uploadImage(uploadCertificate.endpoint, uploadCertificate.token, file.current.files[0]);
        const imageId = uploadImageResp.data.data.id;

        updateAvatar(userId, imageId);
        const imageResp = await getImage(imageId);
        onChangeAvatar(imageResp.data.data);
    };

    return (
        <div className="avatar_wrapper position-relative">
            {!avatar
                ? <img src="/public/logo192.png" width="150" height="150" />
                : <img src={getImageUrl(avatar, ImageSize.MEDIUM)} />
            }
            <button type="button" data-bs-toggle="modal" data-bs-target="#fileModal"
                className={"position-absolute end-0 bottom-0 btn btn-light rounded-circle p-2 me-1 mb-1 " + (editable ? 'd-flex' : 'd-none')}>
                <MdAddAPhoto />
            </button>
            <div className="modal fade" id="fileModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">
                                Uploading a new photo</h5> <button type="button" className="btn-close"
                                    data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <p className="body-desc">
                                It will be easier for your friends to recognize you if you upload your real photo. You can upload the image in JPG, GIF or PNG format. </p>
                            <div className="photo-input">
                                <input type="file" id="loadFile" ref={file} />
                                <button type="button" className="btn btn-primary" onClick={onUpload} data-bs-dismiss="modal"> Upload </button>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <p className="footer-title">
                                If you're having problems uploading, try choosing a smaller photo.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}