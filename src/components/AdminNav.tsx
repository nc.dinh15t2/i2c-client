import { Link } from "react-router-dom";
import { selectUser, useAppSelector } from "../store";
import { RouteUrl } from "../utils";
import { InfoButton } from "./InfoButton";

export function AdminNav() {
    const user = useAppSelector(selectUser);

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <Link className="navbar-brand" to={RouteUrl.ADMIN_DASHBOARD}>Dashboard</Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" 
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <Link className="nav-link active" aria-current="page" to={RouteUrl.ADMIN_USER}>User</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="#">Link</Link>
                        </li>
                    </ul>
                    {
                        !user ?
                            <form className="d-flex ma-2">
                                <Link className="btn btn-outline-primary me-2" to={RouteUrl.LOGIN}> Login  </Link>
                                <Link className="btn btn-outline-success" to={RouteUrl.REGISTER}>Register</Link>
                            </form> :
                            <InfoButton user={user} isAdmin={true}/>
                    }

                </div>
            </div>
        </nav>
    );
}