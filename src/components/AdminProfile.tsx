import { useEffect, useRef, useState } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { FullUser, Image, ImageSize, UploadCertificate, Response } from '../models';
import { getImage, getUserById, getUserInfo, signUploadCertificate, updateAvatar, updateUser, uploadImage } from '../services';
import { selectUser, useAppSelector } from '../store';
import { getImageUrl, PASSWORD_PLACE_HOLDER, UploadType } from '../utils';
import { AiFillEye, AiFillEyeInvisible } from 'react-icons/ai';
import './css/Profile.css';
import { useForm } from 'react-hook-form';
import { AvatarComponent } from './AvatarComponent';

function usePrevious(value) {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}

function useFormula(calc) {
    const ref = useRef();
    useEffect(() => {
        ref.current = calc();
    });
    return ref.current;
}

export function AdminProfile() {
    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const userId = searchParams.get('user_id');

    const [user, setUser] = useState(null as FullUser);
    const [showPassword, setShowPassword] = useState(false);
    const { register, handleSubmit, setValue } = useForm();
    const [avatar, setAvatar] = useState(null as Image);
 
    const prevParams:any = usePrevious(searchParams);
    useEffect(() => {
        if(!prevParams || userId !== prevParams.get('user_id')) {
            getUserById(userId)
                .then(userResp => {
                    setUser(userResp.data);
                    setAvatar(userResp.data.avatar);
                    setValue('name.first', userResp.data.name.first);
                    setValue('name.last', userResp.data.name.last);
                    setValue('gender', userResp.data.gender);
                    setValue('status', userResp.data.status);
                })
                .catch(err => {
                    console.log(err);
                });
        }
    });

    const onSubmit = data => {
        console.log(data);
        updateUser(userId, data).then(userResp => console.log(userResp)).catch(err => console.log(err));
    };

    const onError = errors => {
        console.log(errors);
    };

    return (
        <div className="w-100">
            <form onSubmit={handleSubmit(onSubmit, onError)}>
                <div className="d-flex flex-column text-start w-50 mx-auto mt-4">
                    <div className="d-flex">
                        <div className="me-4">
                            <AvatarComponent avatar={avatar} userId={user?user._id:''} editable={false} onChangeAvatar={(avatar) => setAvatar(avatar)}/>
                        </div>
                        {user
                            ? <div className="text-start w-100">
                                <div className="input-group mt-2 d-flex flex-column">
                                    <label htmlFor="input-email">Email</label>
                                    <input id="input-email" className="form-control w-100" value={user.email} disabled />
                                </div>
                                <div className="input-group mt-2 d-flex flex-column">
                                    <label htmlFor="input-username">Username</label>
                                    <input id="input-username" className="form-control w-100" value={user.username} disabled />
                                </div>
                            </div>
                            : <div>
                            </div>
                        }
                    </div>
                    <div className="text-start">
                        <div className="mt-2 row">
                            <div className="col input-group d-flex flex-column">
                                <label htmlFor="input-first-name">First Name</label>
                                <input id="input-first-name" className="form-control w-100"
                                    {...register('name.first', {

                                    })} />
                            </div>
                            <div className="col input-group d-flex flex-column">
                                <label htmlFor="input-last-name">Last Name</label>
                                <input id="input-last-name" className="form-control w-100"
                                    {...register('name.last', {

                                    })} />
                            </div>
                        </div>
                        <div className="input-group mt-2 d-flex flex-column">
                            <label htmlFor="input-gender">Gender</label>
                            <select className="form-select w-25" id="input-gender"
                                {...register('gender', {

                                })}>
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                                <option value="O">Other</option>
                            </select>
                        </div>
                        <div className="input-group mt-2 d-flex flex-column">
                            <label htmlFor="input-status">Status</label>
                            <select className="form-select w-25" id="input-status"
                                {...register('status', {

                                })}>
                                <option value="A">Active</option>
                                <option value="I">Inactive</option>
                                <option value="B">Blocked</option>
                            </select>
                        </div>
                        <div className={"input-group mt-4 d-flex"} >
                            <button className="btn btn-primary w-100 mx-auto" type="button"
                                data-bs-toggle="modal" data-bs-target="#passwordModal">
                                Update
                            </button>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="passwordModal" aria-labelledby="passwordModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="passwordModalLabel">
                                    Confirm
                                </h5> 
                                <button type="button" className="btn-close" 
                                    data-bs-dismiss="modal" aria-label="Close">    
                                </button>
                            </div>
                            <div className="modal-body">
                                <p className="body-desc">
                                    {'Do you want to edit this profile'}
                                </p>
                                <div className="input-group row ms-0 mt-3">
                                    <button type="submit" data-bs-dismiss="modal" 
                                        className="btn btn-primary col me-4 rounded">OK</button>
                                    <button type="button" data-bs-dismiss="modal"
                                        className="btn btn-secondary col rounded">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    )
}