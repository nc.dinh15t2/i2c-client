import { Link, Router, useHistory } from "react-router-dom";
import { useState } from "react";
import "./css/Login.css";
import { AiFillEye, AiFillEyeInvisible } from 'react-icons/ai';
import { useForm } from "react-hook-form";
import { loginAPI } from "../services";
import Cookies from 'universal-cookie';
import { RouteUrl } from "../utils";

export function Login() {
    const [showPassword, setShowPassword] = useState(false);
    const { register, handleSubmit } = useForm();
    const cookies = new Cookies();

    function onClickShow(e) {
        e.preventDefault();
        setShowPassword(!showPassword);
    }

    const history = useHistory();

    function onLogin(data) {
        loginAPI(data)
            .then(response => {
                if (response.status === 200) {
                    const data = response.data;
                    cookies.set('access_token', data.accessToken);
                    cookies.set('refresh_token', data.refreshToken);
                    history.push(RouteUrl.ADMIN_DASHBOARD);
                }
            })
            .catch(err => {
                console.log(err);
            });
    }

    return (
        <form onSubmit={handleSubmit(onLogin)}>
            <div className="mb-4 text-center">
                <h4> Welcome to I2C </h4>
            </div>
            <div className="mb-3">
                <label htmlFor="input-email" className="form-label">Email address</label>
                <input type="text" className="form-control"
                    id="input-email" placeholder="Username or Email"
                    {...register('identity')} />
            </div>
            <div className="mb-3">
                <label htmlFor="input-password" className="form-label">Password</label>
                <div className="position-relative d-flex">
                    <input type={showPassword ? 'text' : 'password'} className="form-control"
                        id="input-password" placeholder="Password"
                        {...register('password')} />
                    <button className="btn btn-outline position-absolute end-0"
                        onClick={onClickShow}>
                        {!showPassword ? <AiFillEye /> : <AiFillEyeInvisible />}
                    </button>
                </div>
            </div>
            <div className="mb-3">
                <input type="checkbox" id="input-remember-me" className="form-check-input me-1" />
                <label htmlFor="input-remember-me">Remember me</label>
            </div>
            <div className="mb-4 text-center">
                <button className="w-100 btn btn-warning" type="submit">Login</button>
            </div>
            <div className="mb-3 text-center">
                <Link to={ RouteUrl.REGISTER }>Register</Link>
            </div>
        </form>
    )
}