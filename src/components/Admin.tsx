import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { getUserInfo } from '../services';
import { setUser, useAppDispatch } from '../store';
import { RouteUrl } from '../utils';
import { AdminNav } from './AdminNav';
import { useAlert } from 'react-alert';
import './css/Admin.css';

export function Admin(props) {
    const history = useHistory();
    const alert = useAlert();
    const dispatch = useAppDispatch();
    useEffect(() => {
        getUserInfo()
            .then(response => {
                dispatch(setUser(response.data));
            })
            .catch(err => {
                if(!err.status) {
                    console.log(err); 
                    return;  
                }
                if(err.response.status === 401) {
                    alert.info('You must login first', {
                        timeout: 1500
                    });
                    history.push(RouteUrl.LOGIN);
                }
            });
    }, []);

    return (
        <div>
            <AdminNav/>
            { props.children }
        </div>
    );
}