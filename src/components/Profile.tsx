import { useEffect, useRef, useState } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { FullUser, Image, ImageSize, UploadCertificate, Response } from '../models';
import { getImage, getUserById, getUserInfo, signUploadCertificate, updateAvatar, uploadImage } from '../services';
import { selectUser, useAppSelector } from '../store';
import { getImageUrl, PASSWORD_PLACE_HOLDER, UploadType } from '../utils';
import { AiFillEye, AiFillEyeInvisible } from 'react-icons/ai';
import './css/Profile.css';
import { useForm } from 'react-hook-form';
import { AvatarComponent } from './AvatarComponent';

function usePrevious(value) {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}

function useFormula(calc) {
    const ref = useRef();
    useEffect(() => {
        ref.current = calc();
    });
    return ref.current;
}
export function Profile() {
    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);

    const [user, setUser] = useState(null as FullUser);
    const [showPassword, setShowPassword] = useState(false);
    const { register, handleSubmit, setValue } = useForm();
    const [avatar, setAvatar] = useState(null as Image);

    const isOwner = useFormula(() => searchParams.get('user_id') === 'me');
    
    const prevParams:any = usePrevious(searchParams);
    useEffect(() => {
        if(!prevParams || searchParams.get('user_id') !== prevParams.get('user_id')) {
            getUserById(searchParams.get('user_id'))
                .then(userResp => {
                    setUser(userResp.data);
                    setAvatar(userResp.data.avatar);
                    setValue('name.first', userResp.data.name.first);
                    setValue('name.last', userResp.data.name.last);
                    setValue('gender', userResp.data.gender);
                })
                .catch(err => {
                    console.log(err);
                });
        }
    });

    return (
        <div className="w-100">
            <form>
                <div className="d-flex flex-column text-start w-50 mx-auto mt-4">
                    <div className="d-flex">
                        <div className="me-4">
                            <AvatarComponent avatar={avatar} userId={user?user._id:''} editable={isOwner} onChangeAvatar={(avatar) => setAvatar(avatar)}/>
                        </div>
                        {user
                            ? <div className="text-start w-100">
                                <div className="input-group mt-2 d-flex flex-column">
                                    <label htmlFor="input-email">Email</label>
                                    <input id="input-email" className="form-control w-100" value={user.email} disabled />
                                </div>
                                <div className="input-group mt-2 d-flex flex-column">
                                    <label htmlFor="input-username">Username</label>
                                    <input id="input-username" className="form-control w-100" value={user.username} disabled />
                                </div>
                            </div>
                            : <div>
                            </div>
                        }
                    </div>
                    <div className="text-start">
                        <div className="mt-2 row">
                            <div className="col input-group d-flex flex-column">
                                <label htmlFor="input-first-name">First Name</label>
                                <input id="input-first-name" className="form-control w-100"
                                    disabled={!isOwner}
                                    {...register('name.first', {

                                    })} />
                            </div>
                            <div className="col input-group d-flex flex-column">
                                <label htmlFor="input-last-name">Last Name</label>
                                <input id="input-last-name" className="form-control w-100"
                                    disabled={!isOwner}
                                    {...register('name.last', {

                                    })} />
                            </div>
                        </div>
                        <div className={"input-group mt-2 flex-column " + (isOwner? 'd-flex' : 'd-none')}>
                            <label htmlFor="input-new-password">New Password</label>
                            <div className="position-relative d-flex">
                                <input id="input-new-password" className="form-control w-100"
                                    placeholder="New Password"
                                    type={showPassword ? 'text' : 'password'}
                                    {...register('newPassword', {

                                    })} />
                                <button type="button" className="position-absolute end-0 btn btn-outline"
                                    onClick={() => setShowPassword(!showPassword)}>
                                    {!showPassword
                                        ? <AiFillEye />
                                        : <AiFillEyeInvisible />}
                                </button>
                            </div>
                        </div>
                        <div className={"input-group mt-2 flex-column " + (isOwner? 'd-flex' : 'd-none')}>
                            <label htmlFor="input-confirm-password">Confirm Password</label>
                            <input id="input-confirm-password" className="form-control w-100"
                                type={showPassword ? 'text' : 'password'}
                                placeholder="Confirm Password"
                                {...register('confirmPassword', {

                                })} />
                        </div>
                        <div className="input-group mt-2 d-flex flex-column">
                            <label htmlFor="input-gender">Gender</label>
                            <select className="form-select w-25" id="input-gender"
                                disabled={!isOwner}
                                {...register('gender', {

                                })}>
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                                <option value="O">Other</option>
                            </select>
                        </div>
                        <div className={"input-group mt-4 " + (isOwner ? "d-flex" : "d-none")} >
                            <button className="btn btn-primary w-100 mx-auto" type="button"
                                data-bs-toggle="modal" data-bs-target="#passwordModal">
                                Update
                            </button>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="passwordModal" aria-labelledby="passwordModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="passwordModalLabel">
                                    Confirm
                                </h5> 
                                <button type="button" className="btn-close" 
                                    data-bs-dismiss="modal" aria-label="Close">    
                                </button>
                            </div>
                            <div className="modal-body">
                                <p className="body-desc">
                                    { isOwner
                                    ? 'Input your password to confirm'
                                    : 'Do you want to edit this profile'}
                                </p>
                                <div className={"input-group " + (!isOwner? 'd-none': '')}>
                                    <input type="password" className="form-control"/>
                                </div>
                                <div className="input-group row ms-0 mt-3">
                                    <button type="submit" data-bs-dismiss="modal" 
                                        className="btn btn-primary col me-4 rounded">OK</button>
                                    <button type="button" data-bs-dismiss="modal"
                                        className="btn btn-secondary col rounded">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    )
}