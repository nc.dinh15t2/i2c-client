import { Link, Router, useHistory } from "react-router-dom";
import { ImageSize } from "../models";
import { cookies, getImageUrl, RouteUrl } from "../utils";
import './css/InfoButton.css';

export function InfoButton({ user, isAdmin }) {
    const history = useHistory();

    const onLogout = () => {
        cookies.remove('access_token');
        history.push(RouteUrl.LOGIN);
    };

    return (
        <div>
            <div className="dropdown mx-auto info-wrapper">
                <button className="btn btn-outline-primary d-inline-flex align-items-center rounded-pill p-1 dropdown-toggle dropdown-toggle"
                    type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                    <div className="me-2">
                        {user.avatar
                        ? <img src={getImageUrl(user.avatar, ImageSize.SMALL)} className="rounded-circle" />
                        : <img/>}
                    </div>
                    <div className="fw-bold text-dark m-1">{user.name.first}</div>
                </button>
                <ul className="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                    <li><Link className="dropdown-item" to={ RouteUrl.SELF_PROFILE }>Profile</Link></li>
                    <li><Link className="dropdown-item" to="#">Settings</Link></li>
                    <li><hr className="dropdown-divider" /></li>
                    <li><Link className="dropdown-item" to="#" data-bs-toggle="modal" data-bs-target="#exampleModal">Logout</Link></li>
                </ul>
            </div>

            <div className="modal fade" id="exampleModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Remind</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            Do you want to logout?
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={onLogout}>Ok</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}