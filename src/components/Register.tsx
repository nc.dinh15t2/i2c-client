import { Link } from "react-router-dom";
import { useState } from "react";
import "./css/Login.css";
import { AiFillEye, AiFillEyeInvisible } from 'react-icons/ai';
import { useForm } from "react-hook-form";
import { registerAPI } from "../services";
import { RouteUrl } from "../utils";

export function Register() {
    const [showPassword, setShowPassword] = useState(false);
    const { register, handleSubmit } = useForm();

    function onClickShow(e) {
        e.preventDefault();
        setShowPassword(!showPassword);
    }

    function onRegister(data) {
        console.log(data);
        registerAPI(data)
            .then(response => {
                console.log(response);
            })
            .catch(err => {
                console.log(err);
            });
    }

    return (
        <form onSubmit={handleSubmit(onRegister)} className="container">
            <div className="mb-4 text-center">
                <h4> Welcome to I2C </h4>
            </div>
            <div className="mb-3">
                <label htmlFor="input-email" className="form-label">Email address</label>
                <input type="email" className="form-control"
                    id="input-email" placeholder="Email"
                    {...register('email')} />
            </div>
            <div className="mb-3">
                <label htmlFor="input-username" className="form-label">Username</label>
                <input type="text" className="form-control"
                    id="input-username" placeholder="Username"
                    {...register('username')} />
            </div>
            <div className="row mb-3">
                <div className="col-sm">
                    <label htmlFor="input-first-name" className="form-label">First Name</label>
                    <input type="text" className="form-control"
                        id="input-first-name" placeholder="First Name"
                        {...register('name.first')} />
                </div>
                <div className="col-sm">
                    <label htmlFor="input-last-name" className="form-label">Last Name</label>
                    <input type="text" className="form-control"
                        id="input-last-name" placeholder="First Name"
                        {...register('name.last')} />
                </div>
            </div>
            <div className="mb-3">
                <label htmlFor="input-password" className="form-label">Password</label>
                <div className="position-relative d-flex">
                    <input type={showPassword ? 'text' : 'password'} className="form-control"
                        id="input-password" placeholder="Password"
                        {...register('password')} />
                    <button className="btn btn-outline position-absolute end-0"
                        onClick={onClickShow}>
                        {showPassword ? <AiFillEye /> : <AiFillEyeInvisible />}
                    </button>
                </div>
            </div>
            <div className="mb-3">
                <label htmlFor="input-confirm-password" className="form-label">Confirm Password</label>
                <div className="position-relative d-flex">
                    <input type={showPassword ? 'text' : 'password'} className="form-control"
                        id="input-confirm-password" placeholder="Confirm Password"
                        {...register('confirmPassword')} />
                    <button className="btn btn-outline position-absolute end-0"
                        onClick={onClickShow}>
                        {showPassword ? <AiFillEye /> : <AiFillEyeInvisible />}
                    </button>
                </div>
            </div>
            <div className="mb-3">
                <label htmlFor="input-gender" className="form-label">Gender</label>
                <select id="input-gender" className="form-select w-25" defaultValue="O"
                    {...register('gender')}>
                    <option value="O">Other</option>
                    <option value="M">Male</option>
                    <option value="F">Female</option>
                </select>
            </div>
            <div className="mb-3">
                <input type="checkbox" id="input-remember-me" className="form-check-input me-1" />
                <label htmlFor="input-remember-me">Conditions & Privacy</label>
            </div>
            <div className="mb-4 text-center">
                <button className="w-100 btn btn-warning" type="submit">Register</button>
            </div>
            <div className="mb-2 text-center">
                <Link to={ RouteUrl.LOGIN }>Login</Link>
            </div>
        </form>
    )
}