import { Image, ImageSize, Size } from "../models";

export function getImageUrl(image: Image, sizeName: ImageSize): string {
    const size: Size = image[sizeName];
    return [image.domain, 'resources', image.url, size.localName].join('/');
}