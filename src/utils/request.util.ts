import { HeaderName } from "./constant.util";
import { cookies } from "./storage.util";

export function getAuthHeader() {
    const result = {};
    result[HeaderName.AUTHORIZATION] = 'Bearer ' + cookies.get('access_token');
    return result;
}