export const BASE_URL_V1 = process.env.REACT_APP_BASE_URL_V1;

export module RouteUrl {
    export const LOGIN = '/login';
    export const REGISTER = '/register';
    export const ADMIN_USER = '/admin_user';
    export const ADMIN_DASHBOARD = '/admin_dashboard';
    export const ADMIN_PROFILE = '/admin_profile';
    export const SELF_PROFILE = '/profile?user_id=me';
    export const PROFILE = '/profile';
}

export enum UploadType {
    IMAGE = 'image',
}

export module HeaderName {
    export const AUTHORIZATION = 'Authorization';
    export const UPLOAD_TOKEN = 'upload_token';
}

export const PASSWORD_PLACE_HOLDER = new DOMParser().parseFromString('&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;', 'text/html').body.textContent;