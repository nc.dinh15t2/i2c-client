
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import { Admin } from './components/Admin';
import { AdminDashboard } from './components/AdminDashboard';
import { AdminProfile } from './components/AdminProfile';
import { AdminUser } from './components/AdminUser';
import { Auth } from './components/Auth';
import { Login } from './components/Login';
import { Profile } from './components/Profile';
import { Register } from './components/Register';
import { RouteUrl } from './utils';

function App() {

    return (
        <div className="App">
            <BrowserRouter>
                <Switch>
                    <Route path={RouteUrl.ADMIN_USER}>
                        <Admin>
                            <AdminUser/>
                        </Admin>
                    </Route>

                    <Route path={RouteUrl.ADMIN_PROFILE}>
                        <Admin>
                            <AdminProfile/>
                        </Admin>
                    </Route>

                    <Route path={RouteUrl.PROFILE}>
                        <Profile/>
                    </Route>

                    <Route path={RouteUrl.ADMIN_DASHBOARD}>
                        <Admin>
                            <AdminDashboard/>
                        </Admin>
                    </Route>

                    <Route path={RouteUrl.LOGIN}>
                        <Auth>
                            <Login/>
                        </Auth>
                    </Route>

                    <Route path={RouteUrl.REGISTER}>
                        <Auth>
                            <Register/>
                        </Auth>
                    </Route>
                </Switch>
            </BrowserRouter>
        </div>
    );
}

export default App;
