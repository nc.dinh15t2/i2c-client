import axios from "axios";
import { FullUser, Response, ShortUser } from "../models";
import { BASE_URL_V1, getAuthHeader } from "../utils";

export function getUserInfo() {
    return axios.get(`${BASE_URL_V1}/users/me`, {
        headers: getAuthHeader(),
    });
}

export function getUserById(id) {
    return axios.get(`${BASE_URL_V1}/users/${id}`, {
        headers: getAuthHeader(),
    });
}

export function getUsers(page: number, pageSize: number) {
    return axios.get<Response<ShortUser[]>>(`${BASE_URL_V1}/users`, {
        headers: getAuthHeader(),
        params: {
            p: page,
            limit: pageSize
        }
    });
}

export function updateAvatar(userId: number, imageId: string) {
    return axios.put(`${BASE_URL_V1}/users/${userId}/avatars`, { imageId }, {
        headers: getAuthHeader()
    });
}

export function updateUser(id, data) {
    return axios.put(`${BASE_URL_V1}/users/${id}`, data, {
        headers: getAuthHeader()
    });
}