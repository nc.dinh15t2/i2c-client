import axios from "axios";
import { Id, Image, Response, UploadCertificate } from "../models";
import { BASE_URL_V1, getAuthHeader, UploadType } from "../utils";


export function signUploadCertificate(type: UploadType) {
    return axios.post<Response<UploadCertificate>>(`${BASE_URL_V1}/upload_certificates`, { type }, 
    {
        headers: getAuthHeader(),
    });
}

export function uploadImage(url, token, file) {
    const formData = new FormData();
    formData.append('image',file)
    return axios.post<Response<Id>>(url, formData, 
    {
        headers: {
            ...getAuthHeader(),
            'upload_token': token
        }
    });
}

export function getImage(id) {
    return axios.get<Response<Image>>(`${BASE_URL_V1}/images/${id}`);
}