import axios from "axios";
import { BASE_URL_V1 } from "../utils";

export function loginAPI({identity, password}): Promise<any> {
    return axios.post(`${BASE_URL_V1}/auth/login`, {
        identity, 
        password
    });
}

export function registerAPI({email, username, name: {first, last}, password, gender}) {
    return axios.post(`${BASE_URL_V1}/users`, {
        email,
        username,
        name: {
            first,
            last
        },
        password,
        gender
    });
}